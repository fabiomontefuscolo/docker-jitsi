#!/bin/bash
set -x

if [ -z "${XMPP_INTERNAL_MUC_DOMAIN}" ];
then
    export XMPP_INTERNAL_MUC_DOMAIN="${XMPP_MUC_DOMAIN}"
fi

cp -an /var/backups/videobridge /etc/jitsi
tee /etc/jitsi/videobridge/sip-communicator.properties <<EOF
org.ice4j.ice.harvest.DISABLE_AWS_HARVESTER=${DISABLE_AWS_HARVESTER:-true}
org.ice4j.ice.harvest.STUN_MAPPING_HARVESTER_ADDRESSES=meet-jit-si-turnrelay.jitsi.net:443

org.jitsi.videobridge.SINGLE_PORT_HARVESTER_PORT=${JVB_PORT:-10000}
org.jitsi.videobridge.DISABLE_TCP_HARVESTER=${DISABLE_TCP_HARVESTER:-false}
org.jitsi.videobridge.TCP_HARVESTER_PORT=${JVB_TCP_PORT:-4443}

org.jitsi.videobridge.xmpp.user.shard.HOSTNAME=${XMPP_DOMAIN}
org.jitsi.videobridge.xmpp.user.shard.DOMAIN=${XMPP_DOMAIN}
org.jitsi.videobridge.xmpp.user.shard.USERNAME=${JVB_AUTH_USER}
org.jitsi.videobridge.xmpp.user.shard.PASSWORD=${JVB_AUTH_PASSWORD}
org.jitsi.videobridge.xmpp.user.shard.MUC_JIDS=${JVB_BREWERY_MUC}@${XMPP_INTERNAL_MUC_DOMAIN}
org.jitsi.videobridge.xmpp.user.shard.MUC_NICKNAME=${HOSTNAME}
org.jitsi.videobridge.xmpp.user.shard.DISABLE_CERTIFICATE_VERIFICATION=true

org.jitsi.videobridge.ENABLE_STATISTICS=true
org.jitsi.videobridge.STATISTICS_TRANSPORT=muc
org.jitsi.videobridge.STATISTICS_INTERVAL=5000
EOF

if [ -d "/entrypoint.d" ];
then
    for extra in /entrypoint.d/*; do
        case "$extra" in
            *.sh)     . "$extra" ;;
        esac
        echo
    done
    unset extra
fi

exec "$@"