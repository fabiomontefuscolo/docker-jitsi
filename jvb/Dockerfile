FROM debian
MAINTAINER Fabio Montefuscolo <fabio.montefuscolo@gmail.com>

ARG JVB_PKG="https://download.jitsi.org/stable/jitsi-videobridge2_2.1-202-g5f9377b9-1_all.deb"
ENV DEBIAN_FRONTEND=noninteractive

ENV XMPP_SERVER=server-fqdn.example.com                    \
    XMPP_DOMAIN=example.com                                \
    XMPP_COMPONENT_PORT=5275                               \
    XMPP_COMPONENT_SECRET=secret-you-create-on-xmpp-server \
    XMPP_MUC_DOMAIN=conference.example.com                 \
    JVB_AUTH_USER=jvb-xmpp-user                            \
    JVB_AUTH_PASSWORD=jvb-xmpp-password                    \
    JVB_BREWERY_MUC=MUST-be-equal-to-the-set-on-jvb        \
    VIDEOBRIDGE_MAX_MEMORY=1024m

RUN apt-get update \
    && apt-get install -y curl openjdk-11-jre-headless procps sudo uuid-runtime \
    && curl -o /tmp/jvb.deb -L "${JVB_PKG}" \
    && dpkg -i /tmp/jvb.deb \
    && rm -rf /tmp/jvb.deb /var/lib/apt/lists/* \
    && cp -a /etc/jitsi/videobridge /var/backups/videobridge

ADD ./entrypoint.sh /entrypoint.sh
ADD ./log4j2.xml /etc/jitsi/videobridge/log4j2.xml

VOLUME ["/entrypoint.d/", "/etc/jitsi/videobridge"]

ENTRYPOINT ["/entrypoint.sh"]
CMD sudo -E -u jvb java \
        -Xmx${VIDEOBRIDGE_MAX_MEMORY} \
        -Dnet.java.sip.communicator.SC_HOME_DIR_LOCATION=/etc/jitsi \
        -Dnet.java.sip.communicator.SC_HOME_DIR_NAME=videobridge \
        -Dnet.java.sip.communicator.SC_LOG_DIR_LOCATION=/var/log/jitsi \
        -Djava.util.logging.config.file=/etc/jitsi/videobridge/logging.properties \
        -cp /usr/share/jitsi-videobridge/jitsi-videobridge.jar:/usr/share/jitsi-videobridge/lib/* \
        org.jitsi.videobridge.Main \
            --host=${XMPP_SERVER} \
            --domain=${XMPP_DOMAIN} \
            --secret=${XMPP_COMPONENT_SECRET} \
            --port=${XMPP_COMPONENT_PORT} \
            --apis=none
