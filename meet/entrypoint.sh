#!/bin/bash
set -x

if [ -f "/etc/jitsi/meet/config.js" ]
then
    node <<'EOF'
var fs = require('fs');
eval(fs.readFileSync('/etc/jitsi/meet/config.js') + '');

if (process.env.XMPP_DOMAIN) {
    config.hosts.domain = process.env.XMPP_DOMAIN;
}

if (process.env.XMPP_MUC_DOMAIN) {
    config.hosts.muc = process.env.XMPP_MUC_DOMAIN;
}

if (process.env.XMPP_BOSH_URL) {
    config.bosh = process.env.XMPP_BOSH_URL;
}

if (process.env.XMPP_WEBSOCKET_URL) {
    config.websocket = process.env.XMPP_WEBSOCKET_URL;
}

if (process.env.JICOFO_AUTH_USER) {
    config.focusUserJid = process.env.JICOFO_AUTH_USER + '@' + config.hosts.domain;
} else {
    config.focusUserJid = 'focus@' + config.hosts.domain;
}

if (process.env.ENABLE_RECORDING) {
    config.fileRecordingsEnabled = true;
    config.liveStreamingEnabled = true;
}

if (process.env.USE_NICKS === undefined || process.env.USE_NICKS) {
    config.useNicks = true;
}

if (! process.env.P2P_STUNSERVERS) {
    config.p2p.enabled = true;
    config.p2p.stunServers = [
        { urls: 'stun:stun.l.google.com:19302' },
        { urls: 'stun:stun1.l.google.com:19302' },
        { urls: 'stun:stun2.l.google.com:19302' }
    ];
}

fs.writeFileSync(
    '/opt/jitsi-meet/config.js',
    'var config = ' + JSON.stringify(config, null, 2) + ';'
);
EOF
fi

if [ -d "/entrypoint.d" ];
then
    for extra in /entrypoint.d/*; do
        case "$extra" in
            *.sh)     . "$extra" ;;
        esac
        echo
    done
    unset extra
fi

exec "$@"
