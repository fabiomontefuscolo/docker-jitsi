#!/bin/bash
if [ -z "${XMPP_INTERNAL_MUC_DOMAIN}" ];
then
    export XMPP_INTERNAL_MUC_DOMAIN="${XMPP_MUC_DOMAIN}"
fi

mkdir -p /etc/jitsi/jicofo
tee /etc/jitsi/jicofo/sip-communicator.properties <<EOF
org.jitsi.jicofo.auth.URL=XMPP:${XMPP_DOMAIN}
org.jitsi.jicofo.ALWAYS_TRUST_MODE_ENABLED=true
org.jitsi.jicofo.BRIDGE_MUC=${JVB_BREWERY_MUC}@${XMPP_INTERNAL_MUC_DOMAIN}
org.jitsi.jicofo.health.ENABLE_HEALTH_CHECKS=${ENABLE_HEALTH_CHECKS:-false}
EOF
chown -R jicofo:jicofo /etc/jitsi/jicofo

if [ -d "/entrypoint.d" ];
then
    for extra in /entrypoint.d/*; do
        case "$extra" in
            *.sh)     . "$extra" ;;
        esac
        echo
    done
    unset extra
fi

exec "$@"